/*
 * laser.c
 *
 *  Created on: Sep 9, 2020
 *      Author: sando
 */
#include <stdlib.h>
#include "drivers/laserDriver.h"

struct LaserDriver_s {
	unsigned short enabled;
};

LaserDriver* LaserDriver__new() {
	LaserDriver* this = malloc(sizeof(LaserDriver));
	this->enabled = 0;
	return this;
}

void LaserDriver__delete(LaserDriver* this) {
	free(this);
}
