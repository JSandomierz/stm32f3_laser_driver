/*
 * motorDriver.h
 *
 *  Created on: Sep 9, 2020
 *      Author: sando
 */

#ifndef INC_DRIVERS_MOTORDRIVER_H_
#define INC_DRIVERS_MOTORDRIVER_H_

struct MotorDriver_s;
typedef struct MotorDriver_s MotorDriver;

MotorDriver* MotorDriver__new();
void MotorDriver__delete(MotorDriver* this);

/*
 * 1 - clockwise
 * -1 - ccw
 */
void MotorDriver__setDirection(MotorDriver* this, int direction);

void MotorDriver__step(MotorDriver* this, unsigned int numSteps, unsigned int msecs);

#endif /* INC_DRIVERS_MOTORDRIVER_H_ */
