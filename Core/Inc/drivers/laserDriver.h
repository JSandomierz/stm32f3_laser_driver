/*
 * laserDriver.h
 *
 *  Created on: Sep 9, 2020
 *      Author: sando
 */

#ifndef INC_DRIVERS_LASERDRIVER_H_
#define INC_DRIVERS_LASERDRIVER_H_

struct LaserDriver_s;
typedef struct LaserDriver_s LaserDriver;

LaserDriver* LaserDriver__new();
void LaserDriver__delete(LaserDriver* this);

#endif /* INC_DRIVERS_LASERDRIVER_H_ */
